package aop.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class NewLoggingAspect {

    /* С помощью @Around Advice возможно:
    * 1. произвести какие-либо действия до работы target метода
    * 2. произвести какие-либо действия после работы target метода
    * 3. получить результат работы target метода/изменить его
    * 4. предпринять какие-либо действия, если из target метода выбрасываются исключения
    *
    *  @Around("execution(* *(..))")
    *  public Object aroundReturnBookLoggingAdvice(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
    *      Object targetMethodResult = proceedingJoinPoint.proceed();
    *      return targetMethodResult;
    *  }
    *
    * Используя @Around Advice возможно предпринять следующие действия,
    * если из target метода выбрасывается исключение:
    * - ничего не делать
    * - обрабатывать исключение
    * - пробрасывать исключение дальше
    * */

    @Around("execution(public String returnBook())")
    public Object aroundReturnBookLoggingAdvice(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        System.out.println("aroundReturnBookLoggingAdvice: в библиотеку пытаются вернуть книгу");

        Object targetMethodResult = null;
        try {
            targetMethodResult = proceedingJoinPoint.proceed();
        } catch (Exception e) {
            System.out.println("aroundReturnBookLoggingAdvice: было залогировано исключение " + e);
           // targetMethodResult = "Неизвестное название книги";
            throw e;
        }

        System.out.println("aroundReturnBookLoggingAdvice: в библиотеку успешно вернули книгу");
        return targetMethodResult;
    }
}
