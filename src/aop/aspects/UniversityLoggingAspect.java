package aop.aspects;

import aop.Student;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Aspect
public class UniversityLoggingAspect {

//    @Before("aop.aspects.MyPointcuts.getStudentsMethod()")
//    public void beforeGetStudentLoggingAdvice() {
//        System.out.println("beforeGetStudentLoggingAdvice : logging getting a list of students BEFORE the method getStudent");
//    }

    /* @AfterReturning Advice выполняется только после нормального окончания метода с основной логикой,
    * но до присвоения результата этого метода какой-либо переменной. Поэтому с помощью @AfterReturning Advice
    * возможно изменять возвращаемый результат метода
    *
    * @AfterReturning(pointcut = "execution(* *(..))",
    *                 returning = "students")
    * public void afterReturningGetStudentsLoggingAdvice(JoinPoint joinPoint,
    *                                                    List<Students> students) {you code}
    * */

//    @AfterReturning(pointcut = "aop.aspects.MyPointcuts.getStudentsMethod()",
//                    returning = "students")
//    public void afterReturningGetStudentLoggingAdvice(List<Student> students) {
//        /*
//        Task : изменить значения students
//        */
//        Student firstStudent = students.get(0);
//
//        String nameSurname = firstStudent.getNameSurname();
//        nameSurname = "Mr. " + nameSurname;
//        firstStudent.setNameSurname(nameSurname);
//
//        double avgGrade = firstStudent.getAvgGrade();
//        avgGrade = avgGrade+1;
//        firstStudent.setAvgGrade(avgGrade);
//
//        System.out.println("afterReturningGetStudentLoggingAdvice : logging getting a list of students AFTER the method getStudent");
//    }

//    @AfterThrowing("aop.aspects.MyPointcuts.getStudentsMethod()")
//    public void afterThrowingGetStudentsLoggingAdvice() {
//        System.out.println("afterThrowingGetStudentsLoggingAdvice: logging exception");
//    }

    /* @AfterThrowing Advice не влияет на протекание программы при выбрасывании исключения.
    * с помощью @AfterThrowing Advice можно получить доступ к исключению,
    * которое выбросилось из метода с основной логикой
    *
    * @AfterThrowing(pointcut = "execution(* *(..))",
    *                throwing = "exception")
    * public void afterReturningGetStudentsLoggingAdvice(JoinPoint joinPoint,
    *                                                    Throwable exception) {you code}
    *
    * */

//    @AfterThrowing(pointcut = "aop.aspects.MyPointcuts.getStudentsMethod()",
//                   throwing = "exception")
//    public void afterThrowingGetStudentsLoggingAdvice(Throwable exception) {
//        System.out.println("afterThrowingGetStudentsLoggingAdvice: logging exception" + exception);
//    }

    /* С помощью @After Advice невозможно:
    * 1.получить доступ к исключению, которое выбросилось изметода с основной логикой
    * 2.получить доступ к возвращаемому методом результату
    *
    * @After("execution(* *(..))")
    * public void afterGetStudentsLoggingAdvice(JoinPoint joinPoint) {you code}
    * */

    @After("aop.aspects.MyPointcuts.getStudentsMethod()")
    public void afterGetStudentsLoggingAdvice() {
        System.out.println("afterThrowingGetStudentsLoggingAdvice: registering a normal shutdown method or registering an exception");
    }
}
