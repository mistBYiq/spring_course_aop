package aop.aspects;

import aop.Book;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Aspect // скачиваем библиотеку aspectj weaver из maven repository и подключаем её к проекту
@Order(1) //устанавливаем порядок выполнения Advice. значения могут быть любые(10/20/30 100/200/300 10/30/30), выполняются по порядку
public class LoggingAspect {

    // Advice это метод, который находится в Aspect классе, содержит сквозную логику, и определяет что должно произойти при вызове метода
    /*
     * Pointcut - выражение, описывающее где должен быть применен Advice
     * типы Advice:
     * Before - выполняется до метода с основной логикой
     * After returning - выполняется только после нормального окончания метода с основной логикой
     * After throwing - выполняется после окончания метода с основной логикой только, если было выброшено исключение
     * After/After finally - выполняется после окончания метода с основной логикой
     * Around -  выполняется и до и после метода с основной логикой
     */

      /* Параметры Pointcut:
    execution(public void *(*)) - * - один параметр   с access modifier = public, c return type = void, и любым именем
    execution(public void getBook(..)) - .. - любое количество параметров) с access modifier = public,
                                                c return type = void, и именем = getBook
    execution(public void getBook(String)) - String/Long/int... - параметр определенного типа
    execution(public void getBook(aop.Book)) - определенного класса. (Нужно указывать полный путь(все пакеты))
    execution(public void getBook(aop.Book, ..))
    execution(* *(..)) - соответсвует любому метотоду с любым количеством любого типа параметров, где бы он ни находился с любым access modifier, любым return type, и любым именем
    */

    /*Join Point - это точка/момент в выполняемой программе когда следует применить Advice
    * т.е. точка переплетения метода с бизнес-логикой и метода со служебным функционалом.
    * Прописав Join Point в параметре метода Advice, мы получаем доступ к информации о сигнатуре
    * и параметрах метода с биснес логикой
    * */

//    @Before("aop.aspects.MyPointcuts.allGetMethods()") // в скобках Pointcut
//    public void beforeGetLoggingAdvice() {
//        System.out.println("beforeGetLoggingAdvice: попытка получить книги/журнала");
//        System.out.println("---------------------------------");
//    }

    @Before("aop.aspects.MyPointcuts.allAddMethods()")
    public void beforeAddLoggingAspect(JoinPoint joinPoint) {

        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        System.out.println("methodSignature = " + methodSignature);
        System.out.println("methodSignature.getMethod() = " + methodSignature.getMethod());
        System.out.println("methodSignature.getReturnType() = " + methodSignature.getReturnType());
        System.out.println("methodSignature.getName() = " + methodSignature.getName());

        /*Task: для метода addBook нужно выводить параметры, а для addMagazine - не выводить даже если параметры есть */
        if (methodSignature.getName().equals("addBook")) {
            Object[] arguments = joinPoint.getArgs();
            for (Object object : arguments ) {
                if (object instanceof Book) {
                    Book myBook = (Book) object;
                    System.out.println("Info book: nameBook = " + myBook.getName()
                                     + ", author book = " + myBook.getAuthor()
                                     + ", year of publication = " + myBook.getYearOfPublication());
                } else if (object instanceof String) {
                    System.out.println("book add in library - " + object);
                }
            }
        }

        System.out.println("beforeGetLoggingAdvice: попытка ADD книги/журнала");
        System.out.println("---------------------------------");
    }

}
