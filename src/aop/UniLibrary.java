package aop;

import org.springframework.stereotype.Component;

@Component
public class UniLibrary extends AbstractLibrary {

    public void getBook() {
        System.out.println("мы берем книгу из UniLibrary  ");
        System.out.println("---------------------------------");
    }

    public String returnBook() {
        int a = 10/0; // for exception
        System.out.println("Мы возвращаем книгу в UniLibrary");
        return "War and Peace";
    }

    public void getMagazine() {
        System.out.println("мы берем журнал из UniLibrary");
        System.out.println("---------------------------------");
    }

    public void returnMagazine() {
        System.out.println("Мы возвращаем журнал в UniLibrary");
        System.out.println("---------------------------------");
    }

    public void addBook(String person_name, Book book) {
        System.out.println("Добавляем книгу в библиотеку");
         System.out.println("---------------------------------");
    }

    public void addMagazine() {
        System.out.println("Добавляем журнал в библиотеку");
         System.out.println("---------------------------------");
    }
}
